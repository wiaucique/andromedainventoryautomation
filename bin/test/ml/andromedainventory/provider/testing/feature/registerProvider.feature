Feature: Register Provider

  Scenario: Successful registry
    Given I am in registry user page using my credentials
    And I am use my credentials
      | Usuario | contrasena |
      | Admin1  | Admin1     |
    And go to provider
    When I am insert data
      | nit       | nombre         | direccion     | telefono  |
      | 890456133 | Alquiler P SAS | Cra 101 82 48 | 320456790 |
    Then I validate "Se ha Agregado el Proveedor Correctamente" message
