package ml.andromedainventory.inventory.testing.models;

public class Inventory {
	
	String nombre;
	String modelo;
	String serial;
	String fechaIngreso;
	String fechaSalida;
	
	//Constructor
	public Inventory(String nombre, String modelo, String serial, String fechaIngreso,String fechaSalida ) {
		super();
		this.nombre = nombre;
		this.modelo = modelo;
		this.serial = serial;
		this.fechaIngreso = fechaIngreso;
		this.fechaSalida = fechaSalida;
	}
	
	//Getters
	public String getNombre() {
		return nombre;
	}
	public String getModelo() {
		return modelo;
	}
	public String getSerial() {
		return serial;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public String getFechaSalida() {
		return fechaSalida;
	}	
	
	

}
