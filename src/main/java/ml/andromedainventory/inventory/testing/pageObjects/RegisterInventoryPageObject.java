package ml.andromedainventory.inventory.testing.pageObjects;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://andromedainventory.ml:7100/")
public class RegisterInventoryPageObject extends PageObject{
	
	//Localizadores
	By linkInventory = By.xpath("//a[@href='../Vista/listaInventario.php']");
	By btnAdd = By.xpath("//span[@class='icon-plus']");
	By listUsuario = By.id("asignado");
	By txtNombre = By.id("nombre");
	By listTipo = By.id("tipo");
	By listMarca = By.id("marca");
	By txtModelo = By.id("modelo");
	By txtSerial = By.id("serial");
	By listFabricante = By.id("fabricante");
	By listEstado = By.id("estado");
	By listProveedor = By.id("proveedor");
	By dateIngreso = By.id("fechaIngreso");
	By dateSalida = By.id("fechaSalida");
	By btnRegistry = By.xpath("//button[text()='Registrar']");
	By txtConfirmText = By.xpath("//h3[text()='Se ha Agregado el Activo Correctamente']");
	
	//Metodos
	
	public void irInventario() {
		getDriver().findElement(linkInventory).click();
	}
	
	public void agregarInventario() {
		getDriver().findElement(btnAdd).click();		
	}
	
	public void seleccionarUsuario() {
		Select usuario = new Select(getDriver().findElement(listUsuario));
		usuario.selectByValue("4");
	}
	
	public void escribirNombre(String nombre) {
		getDriver().findElement(txtNombre).sendKeys(nombre);
	}
	
	public void seleccionarTipo() {
		Select tipo = new Select(getDriver().findElement(listTipo));
		tipo.selectByValue(" Laptop ");
	}
	
	public void seleccionarMarca() {
		Select marca = new Select(getDriver().findElement(listMarca));
		marca.selectByValue(" COMPAQ ");
	}
	
	public void escribirModelo(String modelo) {
		getDriver().findElement(txtModelo).sendKeys(modelo);
	}
	
	public void escribirSerial(String serial) {
		getDriver().findElement(txtSerial).sendKeys(serial);
	}
	
	public void seleccionarFabricante() {
		Select fabricante = new Select(getDriver().findElement(listFabricante));
		fabricante.selectByValue(" COMPAQ ");
	}
	
	public void seleccionarProveedor() {
		Select proveedor = new Select(getDriver().findElement(listProveedor));
		proveedor.selectByValue("3");
	}
	
	public void escribirFechaingreso(String fechaIngreso) {
		getDriver().findElement(dateIngreso).sendKeys(fechaIngreso);
	}
	
	public void escribirFechaSalida(String fechaSalida) {
		getDriver().findElement(dateSalida).sendKeys(fechaSalida);
	}
	
	public void registrar() {
		getDriver().findElement(btnRegistry).click();
	}
	
	public void confirmarTexto() {
		assertThat(getDriver().findElement(txtConfirmText).isDisplayed(), Matchers.is(true));
	}	


}
