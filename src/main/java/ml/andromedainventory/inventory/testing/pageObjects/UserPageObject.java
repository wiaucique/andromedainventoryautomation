package ml.andromedainventory.inventory.testing.pageObjects;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://andromedainventory.ml:7100/")
public class UserPageObject extends PageObject{

	//Localizadores
	By txtUser = By.name("login");
	By txtPassword = By.name("pass");
	By btnLogin = By.className("login-button");
	
	//Metodos
	public void escribirUsuario(String usuario) {
		getDriver().findElement(txtUser).sendKeys(usuario);
	}
	
	public void escribirContrasena(String contrasena) {
		getDriver().findElement(txtPassword).sendKeys(contrasena);
	}
	
	public void iniciarSesion() {
		getDriver().findElement(btnLogin).click();
	}
	
	
}
