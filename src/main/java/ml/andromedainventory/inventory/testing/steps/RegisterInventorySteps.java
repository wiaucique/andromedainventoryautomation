package ml.andromedainventory.inventory.testing.steps;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.openqa.selenium.support.ui.Select;

import ml.andromedainventory.inventory.testing.pageObjects.RegisterInventoryPageObject;
import net.thucydides.core.annotations.Step;

public class RegisterInventorySteps {
	
	//objeto para usar atributos
	RegisterInventoryPageObject registerPOM = new RegisterInventoryPageObject();
	
	//Step
	@Step
	public void abrirApp() {
		registerPOM.open();
	}
	@Step
	public void irInventario() {
		registerPOM.irInventario();		
	}
	@Step
	public void agregarInventario() {
		registerPOM.agregarInventario();		
	}
	@Step
	public void seleccionarUsuario() {
		registerPOM.seleccionarUsuario();
	}
	@Step
	public void escribirNombre(String nombre) {
		registerPOM.escribirNombre(nombre);
	}
	@Step
	public void seleccionarTipo() {
		registerPOM.seleccionarTipo();
	}
	@Step
	public void seleccionarMarca() {
		registerPOM.seleccionarMarca();
	}
	@Step
	public void escribirModelo(String modelo) {
		registerPOM.escribirModelo(modelo);
	}
	@Step
	public void escribirSerial(String serial) {
		registerPOM.escribirSerial(serial);
	}
	@Step
	public void seleccionarFabricante() {
		registerPOM.seleccionarFabricante();
	}
	@Step
	public void seleccionarProveedor() {
		registerPOM.seleccionarProveedor();
	}
	@Step
	public void escribirFechaingreso(String fechaIngreso) {
		registerPOM.escribirFechaingreso(fechaIngreso);
	}
	@Step
	public void escribirFechaSalida(String fechaSalida) {
		registerPOM.escribirFechaSalida(fechaSalida);
	}
	@Step
	public void registrar() {
		registerPOM.registrar();
	}
	@Step
	public void confirmarTexto() {
		registerPOM.confirmarTexto();
	}
	

}
