package ml.andromedainventory.provider.testing.models;

public class Provider {
	
	String nit;
	String nombre;
	String direccion;
	String telefono;
	
	//Constructor
	public Provider(String nit, String nombre, String direccion, String telefono) {
		super();
		this.nit = nit;
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
	}

	//Getters
	public String getNit() {
		return nit;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public String getTelefono() {
		return telefono;
	}
	
	
	
	

}
