package ml.andromedainventory.provider.testing.models;

public class User {
	
	String usuario;
	String contrasena;
	
	// constructor
	
	public User(String usuario, String contrasena) {
		super();
		this.usuario = usuario;
		this.contrasena = contrasena;
	}
	
	//Getters

	public String getUsuario() {
		return usuario;
	}

	public String getContrasena() {
		return contrasena;
	}
	

}
