package ml.andromedainventory.provider.testing.pageObjects;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://andromedainventory.ml:7100/")
public class RegisterProviderPageObject extends PageObject {

	//Localizadores
	By linkProvider = By.xpath("//a[@href='../Vista/listaProveedor.php']");
	By btnAdd = By.xpath("//span[@class='icon-plus']");
	By txtNit = By.id("Nit");
	By txtNombreProveedor = By.id("NombreProveedor");
	By txtDireccionProveedor = By.id("DireccionProveedor");
	By txtTelefonoProveedor = By.id("TelefonoProveedor");
	By btnRegistry = By.xpath("//button[text()='Registrar']");
	By txtConfirmText = By.xpath("//h3[text()='Se ha Agregado el Proveedor Correctamente']");
	
	//Metodos
	
	public void irProveedores() {
		getDriver().findElement(linkProvider).click();
	}
	
	public void agregarProveedor() {
		getDriver().findElement(btnAdd).click();
	}
	
	public void escribirNit(String nit) {
		getDriver().findElement(txtNit).sendKeys(nit);
	}
	
	public void escribirNombre(String nombre) {
		getDriver().findElement(txtNombreProveedor).sendKeys(nombre);;
	}
	
	public void escribirDireccion(String direccion) {
		getDriver().findElement(txtDireccionProveedor).sendKeys(direccion);
	}
	
	public void escribirTelefono(String telefono) {
		getDriver().findElement(txtTelefonoProveedor).sendKeys(telefono);
	}
	
	public void registrar() {
		getDriver().findElement(btnRegistry).click();
	}
	
	public void confirmarTexto() {
		assertThat(getDriver().findElement(txtConfirmText).isDisplayed(), Matchers.is(true));
	}	
	
}
