package ml.andromedainventory.provider.testing.steps;

import ml.andromedainventory.provider.testing.pageObjects.RegisterProviderPageObject;
import net.thucydides.core.annotations.Step;

public class RegisterProviderSteps {
	
	//objeto para usar atributos
	RegisterProviderPageObject registerPOM = new RegisterProviderPageObject();
	
	//Step
	@Step
	public void irProveedores() {
		registerPOM.irProveedores();
	}
	
	@Step
	public void agregarProveedor() {
		registerPOM.agregarProveedor();
	}
	
	@Step
	public void escribirNit(String nit) {
		registerPOM.escribirNit(nit);
	}
	
	@Step
	public void escribirNombre(String nombre) {
		registerPOM.escribirNombre(nombre);
	}
	@Step
	public void escribirDireccion(String direccion) {
		registerPOM.escribirDireccion(direccion);
	}
	@Step
	public void escribirTelefono(String telefono) {
		registerPOM.escribirTelefono(telefono);
	}
	@Step
	public void registrar() {
		registerPOM.registrar();
	}
	@Step
	public void confirmarTexto() {
		registerPOM.confirmarTexto();
	}

}
