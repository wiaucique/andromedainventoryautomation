package ml.andromedainventory.provider.testing.steps;

import ml.andromedainventory.users.testing.pageObjects.UserPageObject;
import net.thucydides.core.annotations.Step;

public class UserSteps {
	
	//objeto para usar atributos
	UserPageObject userPOM =  new UserPageObject();
	
	//Step
	@Step
	public void abrirApp() {
		userPOM.open();
	}
	
	@Step
	public void escribirUsuario(String usuario) {
		userPOM.escribirUsuario(usuario);
	}
	
	@Step
	public void escribirContrasena(String contrasena) {
		userPOM.escribirContrasena(contrasena);
	}
	
	@Step
	public void iniciarSesion() {
		userPOM.iniciarSesion();
	}
	
}
