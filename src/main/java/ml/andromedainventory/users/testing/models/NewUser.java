package ml.andromedainventory.users.testing.models;

public class NewUser {
	
	String nombres;
	String apellidos;
	String cedula;
	String cargo;
	String area;
	String correo;
	
	// Constructor
	
	public NewUser(String nombres, String apellidos, String cedula, String cargo, String area, String correo) {
		super();
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.cedula = cedula;
		this.cargo = cargo;
		this.area = area;
		this.correo = correo;
	}
	
	// Getters

	public String getNombres() {
		return nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public String getCedula() {
		return cedula;
	}

	public String getCargo() {
		return cargo;
	}

	public String getArea() {
		return area;
	}

	public String getCorreo() {
		return correo;
	}
	

	
}
