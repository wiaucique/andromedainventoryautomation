package ml.andromedainventory.users.testing.pageObjects;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://andromedainventory.ml:7100/")
public class RegisterUserPageObject extends PageObject {

	//Localizadores
	By linkUser = By.xpath("//a[@href='../Vista/listaUsuarios.php']");
	By btnAdd = By.xpath("//span[@class='icon-add-user']");
	By txtNames = By.id("Nombres");
	By txtLastnames = By.id("Apellidos");
	By txtCedula = By.id("Cedula");
	By txtCargo = By.id("Cargo");
	By txtArea = By.id("Area");
	By txtCorreo = By.id("Correo");
	By btnRegistry = By.xpath("//button[@class='btn btn-primary btn-lg']");
	By txtConfirmText = By.xpath("//h3[text()='Se ha Agregado el Usuario Correctamente']");
	
	//Metodos
	
	public void irUsuarios() {
		getDriver().findElement(linkUser).click();
	}
	
	public void agregarUsuario() {
		getDriver().findElement(btnAdd).click();
	}
	
	public void escribirNombres(String nombres) {
		getDriver().findElement(txtNames).sendKeys(nombres);
	}
	
	public void escribirApellidos(String apellidos) {
		getDriver().findElement(txtLastnames).sendKeys(apellidos);
	}
	
	public void escribirCedula(String cedula) {
		getDriver().findElement(txtCedula).sendKeys(cedula);
	}
	
	public void escribirCargo(String cargo) {
		getDriver().findElement(txtCargo).sendKeys(cargo);
	}
	
	public void escribirArea(String area) {
		getDriver().findElement(txtArea).sendKeys(area);
	}
	
	public void escribirCorreo(String correo) {
		getDriver().findElement(txtCorreo).sendKeys(correo);
	}
	
	public void registrar() {
		getDriver().findElement(btnRegistry).click();
	}
	
	public void confirmarTexto() {
		assertThat(getDriver().findElement(txtConfirmText).isDisplayed(), Matchers.is(true));
	}	
	
}
