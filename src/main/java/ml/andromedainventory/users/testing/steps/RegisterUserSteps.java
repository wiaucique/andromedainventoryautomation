package ml.andromedainventory.users.testing.steps;

import ml.andromedainventory.users.testing.pageObjects.RegisterUserPageObject;
import net.thucydides.core.annotations.Step;

public class RegisterUserSteps {

	//objeto para usar atributos
	RegisterUserPageObject registerPOM = new RegisterUserPageObject();
	
	//Step
	@Step
	public void irUsuarios() {
		registerPOM.irUsuarios();
	}
	
	@Step
	public void agregarUsuario() {
		registerPOM.agregarUsuario();
	}
	
	@Step
	public void escribirNombres(String nombres) {
		registerPOM.escribirNombres(nombres);
	}
	
	@Step
	public void escribirApellidos(String apellidos) {
		registerPOM.escribirApellidos(apellidos);
	}
	
	@Step
	public void escribirCedula(String cedula) {
		registerPOM.escribirCedula(cedula);
	}
	
	@Step
	public void escribirCargo(String cargo) {
		registerPOM.escribirCargo(cargo);
	}
	
	@Step
	public void escribirArea(String area) {
		registerPOM.escribirArea(area);
	}
	
	@Step
	public void escribirCorreo(String correo) {
		registerPOM.escribirCorreo(correo);
	}
	
	@Step
	public void registrar() {
		registerPOM.registrar();
	}
	
	@Step
	public void confirmarTexto() {
		registerPOM.confirmarTexto();
	}
	
	
			
}
