package ml.andromedainventory.inventory.testing.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/ml/andromedainventory/inventory/testing/feature/registerInventory.feature",
glue="ml.andromedainventory.inventory.testing.stepdefinitions",
snippets=SnippetType.CAMELCASE)
public class RegisterInventoryRunner {

}
