package ml.andromedainventory.inventory.testing.stepdefinitions;

import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ml.andromedainventory.inventory.testing.models.Inventory;
import ml.andromedainventory.inventory.testing.models.User;
import ml.andromedainventory.inventory.testing.steps.RegisterInventorySteps;
import ml.andromedainventory.users.testing.steps.UserSteps;
import net.thucydides.core.annotations.Steps;

public class RegisterInventoryStepDefinitions {
	
	@Steps
	UserSteps userSteps;
	@Steps
	RegisterInventorySteps registerSteps;
	
	@Given("^I am in registry user page using my credentials$")
	public void iAmInRegistryUserPageUsingMyCredentials() {
		registerSteps.abrirApp(); 
	}


	@Given("^I am use my credentials$")
	public void iAmUseMyCredentials(List<User> userList) {
		userSteps.escribirUsuario(userList.get(0).getUsuario());
		userSteps.escribirContrasena(userList.get(0).getContrasena());
	    userSteps.iniciarSesion();
	}

	@Given("^go to provider$")
	public void goToProvider() {
		registerSteps.irInventario();
		registerSteps.agregarInventario();
	}

	@When("^I am insert data$")
	public void iAmInsertData(List<Inventory> newInventoryList) {
		registerSteps.seleccionarUsuario();
		registerSteps.escribirNombre(newInventoryList.get(0).getNombre());
		registerSteps.seleccionarTipo();
		registerSteps.seleccionarMarca();
		registerSteps.escribirModelo(newInventoryList.get(0).getModelo());
		registerSteps.escribirSerial(newInventoryList.get(0).getSerial());
		registerSteps.seleccionarFabricante();
		registerSteps.seleccionarProveedor();
		registerSteps.escribirFechaingreso(newInventoryList.get(0).getFechaIngreso());
		registerSteps.escribirFechaSalida(newInventoryList.get(0).getFechaSalida());
		registerSteps.registrar();
	}

	@Then("^I validate \"([^\"]*)\" message$")
	public void iValidateMessage(String arg1) {
	    registerSteps.confirmarTexto();
	}


}
