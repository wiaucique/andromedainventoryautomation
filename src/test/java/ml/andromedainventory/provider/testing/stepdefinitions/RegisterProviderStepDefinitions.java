package ml.andromedainventory.provider.testing.stepdefinitions;

import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ml.andromedainventory.provider.testing.models.Provider;
import ml.andromedainventory.provider.testing.models.User;
import ml.andromedainventory.provider.testing.steps.RegisterProviderSteps;
import ml.andromedainventory.users.testing.steps.UserSteps;
import net.thucydides.core.annotations.Steps;

public class RegisterProviderStepDefinitions {
	
	@Steps
	UserSteps userSteps;
	@Steps
	RegisterProviderSteps registerSteps;
	
	@Given("^I am in registry user page using my credentials$")
	public void iAmInRegistryUserPageUsingMyCredentials() {
		userSteps.abrirApp();
	}


	@Given("^I am use my credentials$")
	public void iAmUseMyCredentials(List<User> userList) {
		userSteps.escribirUsuario(userList.get(0).getUsuario());
	    userSteps.escribirContrasena(userList.get(0).getContrasena());
	    userSteps.iniciarSesion();
	}

	@Given("^go to provider$")
	public void goToProvider() {
	    registerSteps.irProveedores();
	    registerSteps.agregarProveedor();
	}

	@When("^I am insert data$")
	public void iAmInsertData(List<Provider> providerList) {
		registerSteps.escribirNit(providerList.get(0).getNit());
		registerSteps.escribirNombre(providerList.get(0).getNombre());
		registerSteps.escribirDireccion(providerList.get(0).getDireccion());
		registerSteps.escribirTelefono(providerList.get(0).getTelefono());
		registerSteps.registrar();
	}

	@Then("^I validate \"([^\"]*)\" message$")
	public void iValidateMessage(String arg1) {
	    registerSteps.confirmarTexto();
	}


}
