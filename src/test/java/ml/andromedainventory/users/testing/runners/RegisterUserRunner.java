package ml.andromedainventory.users.testing.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/ml/andromedainventory/users/testing/feature/registerUser.feature",
glue="ml.andromedainventory.users.testing.stepdefinitions",
snippets=SnippetType.CAMELCASE)
public class RegisterUserRunner {

}
