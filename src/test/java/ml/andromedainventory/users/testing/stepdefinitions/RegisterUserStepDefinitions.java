package ml.andromedainventory.users.testing.stepdefinitions;

import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ml.andromedainventory.users.testing.models.NewUser;
import ml.andromedainventory.users.testing.models.User;
import ml.andromedainventory.users.testing.steps.RegisterUserSteps;
import ml.andromedainventory.users.testing.steps.UserSteps;
import net.thucydides.core.annotations.Steps;

public class RegisterUserStepDefinitions {
	
	@Steps
	UserSteps userSteps;
	@Steps
	RegisterUserSteps registerSteps;
	
	@Given("^I am in registry user page using my credentials$")
	public void iAmInRegistryUserPageUsingMyCredentials() {
	   	userSteps.abrirApp();		
	}


	@Given("^I am use my credentials$")
	public void iAmUseMyCredentials(List<User> userList) {
	    userSteps.escribirUsuario(userList.get(0).getUsuario());
	    userSteps.escribirContrasena(userList.get(0).getContrasena());
	    userSteps.iniciarSesion();	    
	}
	
	@Given("^go to user$")
	public void goToUser() {
		registerSteps.irUsuarios();
		registerSteps.agregarUsuario();
	}

	@When("^I am insert data$")
	public void iAmInsertData(List<NewUser> newUserList) {
		
		registerSteps.escribirNombres(newUserList.get(0).getNombres());
		registerSteps.escribirApellidos(newUserList.get(0).getApellidos());
		registerSteps.escribirCedula(newUserList.get(0).getCedula());
		registerSteps.escribirCargo(newUserList.get(0).getCargo());
		registerSteps.escribirArea(newUserList.get(0).getArea());
		registerSteps.escribirCorreo(newUserList.get(0).getCorreo());
		registerSteps.registrar();
		
	    
	}

	@Then("^I validate \"([^\"]*)\" message$")
	public void iValidateMessage(String arg1) {
	   registerSteps.confirmarTexto();
	}

}
