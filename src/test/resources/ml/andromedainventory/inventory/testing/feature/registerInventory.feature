Feature: Register inventory

  Scenario: Successful registry
    Given I am in registry user page using my credentials
    And I am use my credentials
      | Usuario | contrasena |
      | Admin1  | Admin1     |
    And go to provider
    When I am insert data
      | nombre      | modelo  | serial     | fechaIngreso | fechaSalida |
      | Pc portatil | PCPOR12 | DFGD324542 |     03122020 |    05122020 |
    Then I validate "Se ha Agregado el Activo Correctamente" message
