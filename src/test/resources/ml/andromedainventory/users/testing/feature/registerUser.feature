Feature: Register User

  Scenario: Successful registry
    Given I am in registry user page using my credentials
    And I am use my credentials
      | Usuario | contrasena |
      | Admin1  | Admin1     |
    And go to user
    When I am insert data
      | nombres | apellidos | cedula     | cargo         | area | correo                   |
      | William | silva     | 1014234585 | Automatizador | QA   | william1234567@test.com.co |
    Then I validate "Se ha Agregado el Usuario Correctamente" message
